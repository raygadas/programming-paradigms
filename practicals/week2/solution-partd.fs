﻿module Week2.SolutionPartD
//Exercise 1
//Write a function to calculate the absolute value of a number.

let abs x = if x < 0 then -x else x
//Exercise 2
//Write a function to calculate the length of the hypotenuse of a right-angle triangle using Pythagoras theorem.

let hypotenuse a b = 
    let square x = x * x
    sqrt ((square a) + (square b))

//Exercise 3
//Write a function to calculate if a year is a leap year.

let divisibleBy x by = x % by = 0

let leapYear year =
    if (divisibleBy year 400) then true
    else if (divisibleBy year 100) then false
    else (divisibleBy year 4)

//Exercise 3
//Write a function to calculate the number of days in a particular month.

let daysInMonth month year =
    if (month = 4 || month = 6 || month = 9 || month = 11) then 30
    else if month = 2 then
        if (leapYear year) then 29 else 28
    else 31      

//Exercise 4 (Recursion)
//Write recursive function implementations for the following functions:
//• http://en.wikipedia.org/wiki/Lucas_number

let rec Lucas n =
    if n = 0 then 2
    else if n = 1 then 1
    else (Lucas (n-1)) + (Lucas (n-2))

//• http://en.wikipedia.org/wiki/Ackermann_function

let rec Ackermann m n =
    if m = 0 then n+1
    else if m > 0 && n = 0 then Ackermann (m-1) 1
    else Ackermann (m-1) (Ackermann m (n-1)) 

//• Is http://en.wikipedia.org/wiki/Prime_number ?

let isPrime n =
    let rec divibleByAny potentialFactor =
        if (divisibleBy n potentialFactor) then true
        else if potentialFactor > 2 then divibleByAny (potentialFactor-1) 
        else false
    let maxFactor = int (sqrt (float n))
    not (divibleByAny maxFactor)

//• http://en.wikipedia.org/wiki/Greatest_common_divisor

let rec gcd a b =
    if b = 0 then a
    else gcd b (a % b)

//• http://en.wikipedia.org/wiki/Least_common_multiple

let lcm a b = (abs (a*b)) / (gcd a b)

// [<EntryPoint>]
let main argv = 
    assert ((abs -1) = 1)
    assert ((abs -34) = 34)
    assert ((abs 0) = 0)
    assert ((abs 1) = 1)
    assert ((abs 42) = 42)

    assert ((hypotenuse 14.0 48.0) = 50.0)
    assert ((hypotenuse 21.0 72.0) = 75.0)

    assert ((leapYear 0) = true)
    assert ((leapYear 100) = false)
    assert ((leapYear 400) = true)
    assert ((leapYear 1000) = false)
    assert ((leapYear 2000) = true)
    assert ((leapYear 2004) = true)
    assert ((leapYear 2018) = false)

    assert ((daysInMonth 1 2018) = 31)
    assert ((daysInMonth 2 2018) = 28)
    assert ((daysInMonth 3 2018) = 31)
    assert ((daysInMonth 4 2018) = 30)
    assert ((daysInMonth 5 2018) = 31)
    assert ((daysInMonth 6 2018) = 30)
    assert ((daysInMonth 7 2018) = 31)
    assert ((daysInMonth 8 2018) = 31)
    assert ((daysInMonth 9 2018) = 30)
    assert ((daysInMonth 10 2018) = 31)
    assert ((daysInMonth 11 2018) = 30)
    assert ((daysInMonth 12 2018) = 31)
    assert ((daysInMonth 1 2020) = 31)
    assert ((daysInMonth 2 2020) = 29)
    assert ((daysInMonth 3 2020) = 31)
    assert ((daysInMonth 4 2020) = 30)
    assert ((daysInMonth 5 2020) = 31)
    assert ((daysInMonth 6 2020) = 30)
    assert ((daysInMonth 7 2020) = 31)
    assert ((daysInMonth 8 2020) = 31)
    assert ((daysInMonth 9 2020) = 30)
    assert ((daysInMonth 10 2020) = 31)
    assert ((daysInMonth 11 2020) = 30)
    assert ((daysInMonth 12 2020) = 31)

    assert ((Lucas 0) = 2)
    assert ((Lucas 1) = 1)
    assert ((Lucas 2) = 3)
    assert ((Lucas 3) = 4)
    assert ((Lucas 4) = 7)
    assert ((Lucas 5) = 11)
    assert ((Lucas 6) = 18)
    assert ((Lucas 37 ) = 54018521 )

    assert ((Ackermann 0 0) = 1)
    assert ((Ackermann 1 0) = 2)
    assert ((Ackermann 0 1) = 2)
    assert ((Ackermann 1 1) = 3)
    assert ((Ackermann 4 1) = 65533)
    assert ((Ackermann 2 4) = 11)
    assert ((Ackermann 3 4) = 125)

    assert ((isPrime 0) = false)
    assert ((isPrime 1) = false)
    assert ((isPrime 2) = true)
    assert ((isPrime 3) = true)
    assert ((isPrime 4) = false)
    assert ((isPrime 5) = true)
    assert ((isPrime 6) = false)
    assert ((isPrime 7) = true)
    assert ((isPrime 8) = false)
    assert ((isPrime 32452867) = true)
    assert ((isPrime 32452868) = false)      
    
    assert ((gcd 1 1) = 1)
    assert ((gcd 42 56) = 14)
    assert ((gcd 7966496 314080416) = 32)
    assert ((gcd 24826148 45296490) = 526)
    assert ((gcd 12 0) = 12)
    assert ((gcd 0 0) = 0)
    assert ((gcd 0 9) = 9)

    assert ((lcm 21 6) = 42)
    assert ((lcm 6 10) = 30)
    assert ((lcm 12 30) = 60)
    assert ((lcm 24 300) = 600)

    0 // return an integer exit code
