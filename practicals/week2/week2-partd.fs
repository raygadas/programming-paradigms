﻿module Week2.PartD

// Excercise 1
// Write a function to calculate the absolute value of a number.
let abs x = 
    if x < 0 then x * (-1)
    else x


// Excercise 2
// Write a function to calculate the length of the hypotenuse of a right-angle triangle using Pythagoras theorem.
let hypotenuse = 
    let square = fun x -> x * x
    fun a b -> sqrt(square a + square b)


// Excercise 3 
// Write a function to calculate if a year is a leap year.
let divisibleBy x by = x % by = 0
let leapYear year =
    if (year % 4 = 0) then
        if (year % 100 <> 0) then 
            true
        elif (year % 400 = 0) then
            true
        else 
            false
    else 
        false


// Excercise 4
// Write a function to calculate the number of days in a particular month.
let daysInMonth month year =
    if (month = 4 || month = 6 || month = 9 || month = 11) then 30
    else if month = 2 then
        if (leapYear year) then 29 else 28
    else 31  


// Excercise 5
// Recursive functions are defined in F# as follows
//• http://en.wikipedia.org/wiki/Lucas_number
let rec Lucas n =
    if n = 0 then 2
    else if n = 1 then 1
    else (Lucas (n-1)) + (Lucas (n-2))

//• http://en.wikipedia.org/wiki/Ackermann_function
let rec Ackermann m n =
    if m = 0 then n+1
    else if m > 0 && n = 0 then Ackermann (m-1) 1
    else Ackermann (m-1) (Ackermann m (n-1)) 

//• Is http://en.wikipedia.org/wiki/Prime_number ?
let isPrime n =
    let rec divibleByAny potentialFactor =
        if (divisibleBy n potentialFactor) then true
        else if potentialFactor > 2 then divibleByAny (potentialFactor-1) 
        else false
    let maxFactor = int (sqrt (float n))
    not (divibleByAny maxFactor)

//• http://en.wikipedia.org/wiki/Greatest_common_divisor
let rec gcd a b =
    if b = 0 then a
    else gcd b (a % b)

//• http://en.wikipedia.org/wiki/Least_common_multiple
let lcm a b = (abs (a*b)) / (gcd a b)
