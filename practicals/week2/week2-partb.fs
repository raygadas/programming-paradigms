﻿module Week2.PartB

// Exercise 1 (simple name binding)
let a = 42 //a+3


// Exercise 2 (different value types)
let anInt = 10
let aFloat = 20.0
let aString = "I'm a string!"


// Exercise 3 (simple functions)
let succ x = x + 1


// Exercise 4 (partial function application)
let add x y = x + y
let add4 = add 4


// Exercise 5 (nested helper functions)
let quadruple x =
    let double x =
        x * 2
    double(double x)


// Exercise 6 (higher order functions)
let chrisTest test = 
    test "Chris"
let isMe x = 
    if x = "Chris" then 
        "it is Chris!"
    else 
        "its someonelse"


// Exercise 7 (lambda functions)
let addlambda = (fun x y -> x + y)


// Exercise 8 (inline lambda functions) 
// twoTest toma una funcion como parametro y a esa funcion le pasa un 2
let twoTest test =
         test 2


// Exercise 9 (printing messages)
printfn "hello world from Try F#!"


// Exercise 10 (printing with parameters)
printfn "The answer is %d" 42


