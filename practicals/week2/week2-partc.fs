﻿module Week2.PartC

// Which of the following examples follow the offside rule correctly?
let area r =
    let pi = 3.14159
    2.0 * pi * r

let result = area 40.0
printf "%f" result