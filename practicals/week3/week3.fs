﻿module Week3.PartA

// Excercise 1: Find the number of elements of a list
let rec numElements list =
    match list with 
        | []  -> 0
        | head::tail -> 1 + numElements tail


// Excercise 2: Find the average (mean) of a list of numbers
let getAvg list =
    let n = numElements list
    let sum = List.reduce (+) list
    sum / n


// Excercise 3: Reverse a list
let rec reverse list =
    match list with
    | [] -> []
    | head::tail -> (reverse tail) @ [ head ] 


// Exercise 4: Find the last element of a list
let rec findLast list =
    match list with
    | [] -> failwith "Empty list."
    | head :: [] -> head
    | head :: tail -> findLast tail


// Excercise 5: Find kth element of a list
let rec findKthElement k list = 
    match k, list  with
    | 0, (head::_) -> head
    | k, (head::tail) -> findKthElement (k-1) tail
    | _, [] -> failwith "Not possible"


// Excercise 6: Flatten a list of lists into a list

// ¡¡ DUDA !!
// Excercise 7: Eliminate duplicates in a list
let rec deleteDup list = 
    match list with
    | [] -> []
    | head::tail ->   
        deleteDup (List.filter (fun x -> x <> head) tail)

     

// Excercise 8: Find out if a list is a palindromec
let rec reverse list =
    match list with
    | [] -> []
    | head::tail -> (reverse tail) @ [ head ] 
let rec palindrome list =
    let rev = reverse list
    let result = List.compareWith (fun head1 head2 -> head1 - head2) list rev 
    if result = 0 then true
    else false

// DUDA
// Excercise 9: Sort the elements of a list
let greaterThan a b =
    if a > b then true
    else false

let rec findLessThan num list =
    match list with 
    | [] -> []
    | head::[] -> 
        if ( head <= num ) then
            head
        else 
            []
    | head::tail ->
        if ( head <= num ) then
            head @ (findLessThan num tail)
        else
            findLessThan num tail


let rec sort list =
    match list with
    | [] -> []
    | head::[] -> head
    | head::tail ->
        let leftSide =  (fun head tail -> findLessThan head tail)
        let rightSide = (fun head tail -> findGreaterThan head tail)
        []


          







     





    


